import { faker } from "@faker-js/faker";
import { CartProduct, CartContent } from "@/api/cart/types";

faker.setLocale("ru");

export default {
  async getCartContent(): Promise<CartContent> {
    console.log("Fake GET request");
    const createRandomProduct = () => {
      return {
        name: faker.commerce.productName(),
        price: Number(faker.commerce.price(1000, 10000)),
        count: Number(faker.commerce.price(1, 10, 0)),
        uuid: faker.datatype.uuid(),
      };
    };

    const products: CartProduct[] = [];
    let totalSum = 0;

    Array.from({ length: 10 }).forEach(() => {
      const randomProduct = createRandomProduct();
      totalSum += randomProduct.price;
      products.push(randomProduct);
    });

    return { products, totalSum };
  },
};
