export interface CartProduct {
  name: string;
  price: number;
  count: number;
  uuid: string;
}

export interface CartContent {
  products: CartProduct[];
  totalSum: number;
}
