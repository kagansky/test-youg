import cart from "./cart";
import payment from "./payment";

export default {
  cart,
  payment,
};
