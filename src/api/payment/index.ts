import { faker } from "@faker-js/faker";
import { PaymentFormData } from "@/pages/payment/types";

faker.setLocale("ru");

export default {
  async getPaymentUuid(): Promise<string> {
    console.log("Fake GET request");
    const createRandomUuid = () => {
      return faker.datatype.uuid();
    };

    return createRandomUuid();
  },

  async getPaymentTotalSum(uuid: string): Promise<number> {
    console.log(`Fake POST request with payload { uuid: ${uuid} }`);
    const createRandomPrice = () => {
      return Number(faker.commerce.price(1000, 100000));
    };

    return createRandomPrice();
  },

  async sendPayment(data: PaymentFormData): Promise<void> {
    console.log(
      `Fake POST request with payload { data: ${JSON.stringify(data)} }`
    );
  },
};
