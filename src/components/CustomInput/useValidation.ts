import { Ref, computed } from "vue";
import { ValidationTypeValue, validatorsMap } from "@/helpers/validations";
import { InputValue } from "@/components/CustomInput/types";

export default function useGoToPaymentPage(
  value: Ref<InputValue>,
  validationType: ValidationTypeValue | undefined
) {
  const isNeedValidate = computed(() => {
    if (!validationType) return false;

    return !!value.value;
  });

  const currentValidator = computed(() => {
    if (!validationType) return null;

    return validatorsMap[validationType];
  });

  const isValid = computed(() => {
    if (!validationType || !currentValidator.value) return false;

    return currentValidator.value.method(value.value);
  });

  return {
    isNeedValidate,
    currentValidator,
    isValid,
  };
}
