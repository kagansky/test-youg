export const monthOptions = [
  {
    title: "январь",
    id: "01",
  },
  {
    title: "февраль",
    id: "02",
  },
  {
    title: "март",
    id: "03",
  },
  {
    title: "апрель",
    id: "04",
  },
  {
    title: "май",
    id: "05",
  },
  {
    title: "июнь",
    id: "06",
  },
  {
    title: "июль",
    id: "07",
  },
  {
    title: "август",
    id: "08",
  },
  {
    title: "сентябрь",
    id: "09",
  },
  {
    title: "октябрь",
    id: "10",
  },
  {
    title: "ноябрь",
    id: "11",
  },
  {
    title: "декабрь",
    id: "12",
  },
];

export const yearsOptions = [
  {
    title: "2022",
    id: 2022,
  },
  {
    title: "2023",
    id: 2023,
  },
  {
    title: "2024",
    id: 2024,
  },
  {
    title: "2025",
    id: 2025,
  },
  {
    title: "2026",
    id: 2026,
  },
  {
    title: "2027",
    id: 2027,
  },
];
