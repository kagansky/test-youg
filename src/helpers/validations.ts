export enum ValidationTypeValue {
  CARD = "CARD",
  SECURITY_CODE = "SECURITY_CODE",
}

export function cardValidator(value: string | number | null) {
  if (typeof value !== "string") {
    return false;
  }

  const cleanedNumber = value.replace(/ /g, "");

  const cardReg =
    /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/;

  return cardReg.test(cleanedNumber);
}

export function securityCodeValidator(value: string | number | null) {
  if (typeof value !== "string") {
    return false;
  }
  const securityCodeReg = /^[0-9]{3}$/g;

  return securityCodeReg.test(value);
}

export const validatorsMap = {
  [ValidationTypeValue.CARD]: {
    method: (value: string | number | null) => cardValidator(value),
    message: "Некорректный формат карты",
    mask: ["#### #### #### ####", "#### #### #### ##"],
  },
  [ValidationTypeValue.SECURITY_CODE]: {
    method: (value: string | number | null) => securityCodeValidator(value),
    message: "Некорректный формат кода",
    mask: "###",
  },
};
