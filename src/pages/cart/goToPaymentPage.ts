import { useRouter } from "vue-router";
import API from "@/api";

export default function useGoToPaymentPage() {
  const router = useRouter();

  const goToPaymentPage = async () => {
    try {
      const paymentUuid = await API.payment.getPaymentUuid();

      await router.push({
        name: "payment",
        params: { uuid: paymentUuid },
      });
    } catch (error) {
      console.error(error);
      throw new Error("Can't get new payment uuid");
    }
  };

  return {
    goToPaymentPage,
  };
}
