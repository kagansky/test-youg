import { onMounted, ref } from "vue";
import { CartProduct } from "@/api/cart/types";
import API from "@/api";

export default function useCartContent() {
  const products = ref<CartProduct[]>([]);
  const totalSum = ref(0);

  const getCartContent = async () => {
    try {
      const response = await API.cart.getCartContent();

      products.value = response.products;
      totalSum.value = response.totalSum;
    } catch (error) {
      console.error(error);
      throw new Error("Can't get cart content");
    }
  };

  onMounted(getCartContent);

  return {
    products,
    totalSum,
    getCartContent,
  };
}
