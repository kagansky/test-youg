import { computed } from "vue";
import { PaymentFormData } from "@/pages/payment/types";

export default function useCardCompanyValidator(formData: PaymentFormData) {
  const visaReg = /^4/;
  const masterReg = /^5/;

  const getCardCompanyImgUrl = computed(() => {
    if (!formData.number) return null;

    const cleanedNumber = formData.number.replace(/ /g, "");
    if (visaReg.test(cleanedNumber)) return require("@/assets/img/visa.png");
    if (masterReg.test(cleanedNumber))
      return require("@/assets/img/master.png");

    return null;
  });

  return {
    getCardCompanyImgUrl,
  };
}
