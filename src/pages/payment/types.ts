export interface PaymentFormData {
  number: string | null;
  year: string | null;
  month: string | null;
  fio: string | null;
  securityCode: string | null;
}

export type PaymentFormRefsFieldKey = "fio" | "month" | "year" | "securityCode";
