import { computed } from "vue";

export default function useCardCompanyValidator(number: string | null) {
  const visaReg = /^4[0-9]{6,}$/;
  const masterReg =
    /^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/;

  const getCardCompanyImgUrl = computed(() => {
    if (!number) return null;

    const cleanedNumber = number.replace(" ", "");
    console.log(number);
    console.log(visaReg.test(cleanedNumber));

    if (visaReg.test(cleanedNumber)) return "../../assets/img/visa.png";
    if (masterReg.test(cleanedNumber)) return "../../assets/img/master.png";

    return null;
  });

  return {
    getCardCompanyImgUrl,
  };
}
