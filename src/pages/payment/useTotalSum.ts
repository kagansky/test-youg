import { onMounted, ref } from "vue";
import API from "@/api";

export default function useTotalSum(uuid: string) {
  const totalSum = ref(0);

  const getTotalSum = async () => {
    try {
      totalSum.value = await API.payment.getPaymentTotalSum(uuid);
    } catch (error) {
      console.error(error);
      throw new Error("Can't get cart content");
    }
  };

  onMounted(getTotalSum);

  return {
    totalSum,
  };
}
