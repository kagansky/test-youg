import { createRouter, createWebHistory } from "vue-router";

const CartPage = () => import("@/pages/cart/index.vue");
const PaymentPage = () => import("@/pages/payment/index.vue");

const routes = [
  {
    path: "/",
    redirect: "cart",
  },
  {
    name: "cart",
    path: "/cart",
    component: CartPage,
  },
  {
    name: "payment",
    path: "/payment/:uuid",
    component: PaymentPage,
    props: true,
  },
];

export const router = createRouter({
  history: createWebHistory(),
  routes,
});
